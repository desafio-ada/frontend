# Usa Node.js LTS imagem
FROM node:16-alpine

#cria diretorio para aplicacao
RUN mkdir -p /usr/src/app

#muda para diretorio de aplicacao
WORKDIR /usr/src/app

#copia os arquivos package.json e package-lock.json
COPY package*.json ./

#instala dependencias
RUN npm ci
RUN npm i -g @angular/cli

#copia o resto do codigo
COPY . .

# compila a a aplicação
RUN npm run build --prod

# abre a prota 4200
EXPOSE 4200

#inicia a aplicacao
CMD ["npm", "start"]
